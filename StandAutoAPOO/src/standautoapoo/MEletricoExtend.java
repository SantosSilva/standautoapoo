package standautoapoo;

public class MEletricoExtend extends MotorDadosIN {
    
    private int autonomia;
    
    /**
     *
     * @param powerCV potencia do motor eletrico cv
     * @param autonomia autonomia maxima do veiculo
     */
    public MEletricoExtend(int powerCV , int autonomia){
        super(powerCV, 'e');
        
        this.setAutonomia(autonomia);
    }

    /**
     *
     * @return autonomia do veiculo
     */
    public int getAutonomia(){
        return autonomia;
    }
    
    /**
     *
     * @param autonomia parametros referentes a autonomia
     */
    public void setAutonomia(int autonomia){
        this.autonomia = autonomia;
    }
    
        /**toSting
     *
     * organizaçao do dados necessarios
     * @return
     */
    
    public String toString(){
        return "||| Potencia CV: " + getPowerCV() + "CV ; Autonomia: " + getAutonomia()+ "km |||";
    }
    
    /**
     * 
     * @return parametros do motor eletrico ou do veiculo eletrico
     */
    public String getSpecs() {
        return getPowerCV()+";"+getAutonomia();
    }
}
