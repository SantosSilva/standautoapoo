package standautoapoo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import static standautoapoo.StandAutoAPOO.NVeic;
import static standautoapoo.StandAutoAPOO.stand;

public class InserirVeiculo 
{
private static int NVeic=0;
private static char escolha, ch;
private static String brands, enginetype, Registration, stand;
private static int year;
private static double price;
private static VeiculoDados[] StandVeic = new VeiculoDados[50]; 
private static MotorDadosIN engine = new MotorDadosIN();

private static final Scanner scan = new Scanner(System.in);
    
    /**
     *
     * @param StandLocal parametro da localizaçao do stand
     * @return
     */
    public static String IniciarInserirVeiculo(String StandLocal) { // inserir dados do motor
String sf,sf1;        
                    System.out.println("Insira a Marca");
                    brands = scan.next();    
                    System.out.println("Insira a Matricula");
                    Registration = scan.next();
                    do{
                    System.out.println("Insira o Ano de construção");
                    year = scan.nextInt();
                     }
                    while(year<0 );
                    do{
                    System.out.println("Insira o Preço");
                    price = scan.nextDouble();
                    }
                    while(price<0 );
                    do{
                        System.out.println("Insira o tipo de Motor Combustão(c) Elétrico(e)");
                        sf1 = scan.next();
                        enginetype = sf1.toLowerCase(); 
                    }
                    while(!"e".equals(enginetype) && !"c".equals(enginetype));
                    if (enginetype.equals("c")){ // motor combustao
                        System.out.println("Indique a potencia");                
                        int powerCV = scan.nextInt();
                        System.out.println("Indique a cilindrada"); 
                        int cc = scan.nextInt();
                        System.out.println("Indique o combustivel"); 
                        String fueltype = scan.next();

                        engine = new MCombustivelExtend(powerCV, fueltype, cc);
                    }
                    else{ // motor eletrico
                        System.out.println("Indique a pontencia");                
                        int powerCV = scan.nextInt();
                        System.out.println("Indique a autonomia");  
                        int autonomia = scan.nextInt();

                        engine = new MEletricoExtend(powerCV , autonomia);
                    }
                    StandVeic[NVeic++] = new VeiculoDados(engine, brands, Registration, year, price);
                    SaveDados(StandLocal);
          return null;
                    

}
    
    /**
     *
     * @param StandLocalSave efetuar gravaçao de dados automatica no stand selecionado
     */
    public static void SaveDados(String StandLocalSave){

                    System.out.println("Marca: " +brands+ " Matricula: " +Registration+ " Ano: " +year+ " Motor: [" +engine+ "] Preço: "+price);
          
                    try (FileWriter fw = new FileWriter(StandLocalSave, true);
                        BufferedWriter bw = new BufferedWriter(fw);
                        PrintWriter out = new PrintWriter(bw)) {
                        for (int i=0; i<NVeic; i++){                            
                            VeiculoDados v = StandVeic[i];
                            String linha = v.getBrands()+";"+v.getCarRegistration()+";"+v.getYear()+";"+v.getPriceF()+";"+v.getMotorDadosIN().getEngineType()+";"+v.getMotorDadosIN().getSpecs();
                            //System.out.println(linha);
                            System.out.println("Marca: " +v.getBrands()+ " ; Matricula: " +v.getCarRegistration()+ " ; Ano: " +v.getYear()+ " ; Motor: (" +v.getMotorDadosIN().getEngineType()+ ") Caracteristicas:"+v.getMotorDadosIN().getSpecs() +" ; Preço: "+v.getPriceF());
                            System.out.println("-");
                            out.println(linha);

                        }
                        out.close();
                        bw.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println("O caminho não está correto!");
                    }
    
    }
}