package standautoapoo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class StandAutoAPOO {
    
 //numero de veiculos
public static int NVeic=0,
 
   //numero referente a cada stand
    NumeroStand=0,
        
//tamanho do stand
    TamanhoDados=50,

//ano
    year;

//caracter de escolha
public static char escolher,
    ch;
public static String brands,
    enginetype,// variavel global do tipo de motor
    Registration, // variavel global da matricula do veiculo
    stand,// variavel global do stand
    localizacao; // variavel global da localizaçao do stand
private static String[] NomeStand = {"Stand Tinocar","Stand Nitrocar","Stand Lercio Pinto"}; // variavel global do nome dos stands
 
    public static VeiculoDados[] StandVeic = new VeiculoDados[50]; 
private static MotorDadosIN engine = new MotorDadosIN();


    public static double price;
 
private static final Scanner scan = new Scanner(System.in);

    /**
     *
     * @param args
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        
        while (true){

String path = Paths.get("").toAbsolutePath().toString();//localizaçao do projeto
localizacao=path+"\\"+NomeStand[NumeroStand]+".txt";//localizaçao do stand escolhido
System.out.println("carregar dados"); // escrita

//menu
            System.out.println("\t#########################################################################");
            System.out.println("\t#    TRABALHO FINAL DE APOO - Grupo 17 (Tiago Silva e Rui Ferreira)     #");
            System.out.println("\t#########################################################################");
            System.out.println("\t                     Gestão do "+NomeStand[NumeroStand]);
            System.out.println("\t#########################################################################");
            System.out.println("\t#  (1) Inserir um novo veiculo                                          #");
            System.out.println("\t#  (2) Eliminar veiculo da base dados                                   #");
            System.out.println("\t#  (3) Consultar a lista de veiculos da base dados                      #");
            System.out.println("\t#  (4) Pesquisar um veiculos na base dados                              #");
            System.out.println("\t#  (5) Alterar o Stand                                                  #");
            System.out.println("\t#  (0) Sair                                                             #");
            System.out.println("\t#########################################################################");
            escolher = scan.next().charAt(0);//leitura da opçao

            switch (escolher) {//encaminhamento das escolhas
                case '1' :
                    InserirVeiculoNew();  //chama metodo para inserir novo veiculo
                    break;
                
                case '2' :
                    EliminarVeiculo();//chama metodo para eliminar veiculo
                    break;
                
                case '3' : 
                    Consultar(); //chama metodo para consultar veiculo
                    break;
                
                case '4' :    
                    Pesquisar(); //chama metodo para pesquisar veiculo
                    break;
                    
                case '5' : //troca de stand
                   System.out.println("Troca de Stand\n");
                    if(NumeroStand==0)
                            {
                                NumeroStand=1;
                            }
                    else if(NumeroStand==1)
                            {
                                NumeroStand=2;
                            }
                    else if(NumeroStand==2)
                            {
                                NumeroStand=0;
                            }
                    break;

                case '0' :                    
                    System.exit(0);

            default:  System.out.println("Opção indisponível!");
                    break;        
             }
        }
    }

private static void InserirVeiculoNew() throws IOException{
//chama classe exterior para inserir dados
        InserirVeiculo variavel = new InserirVeiculo();
        variavel.IniciarInserirVeiculo(localizacao);
    }

private static void EliminarVeiculo() throws FileNotFoundException, IOException{
    //metodo para eliminar dados
Scanner ler = new Scanner(System.in);
String DadosEliminar=null;
Consultar(); //chama o metodo para consultar
System.out.println("Selecione a matricula do veiculo a eliminar\n");
String MatriApagar = ler.next();

try {
                         List<String> allLines = Files.readAllLines(Paths.get(localizacao));
                        for (String line : allLines) {
                            String [] vec = line.split(";");
                            VeiculoDados VeiculoD = null;
                            if (vec[1].equals(MatriApagar)){
                                System.out.println("\n"+"Veiculo com a matricula "+MatriApagar+ " foi eliminada");
                                DadosEliminar =line;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("\t"+"Veiculo com a matricula  "+MatriApagar+ " NAO foi eliminada!!!");
                    }

System.out.println(DadosEliminar);

File inputFile = new File(NomeStand[NumeroStand]+".txt");
File tempFile = new File("temp.txt");

BufferedReader reader = new BufferedReader(new FileReader(inputFile));
BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

String currentLine;
String lineToRemove = DadosEliminar;
while((currentLine = reader.readLine()) != null) {
    String trimmedLine = currentLine.trim();
    if(trimmedLine.equals(lineToRemove)) continue;
    writer.write(currentLine + System.getProperty("line.separator"));
}
writer.close(); 
reader.close(); 
InputStream is = null;
    OutputStream os = null;
    try {
        is = new FileInputStream(tempFile);
        os = new FileOutputStream(inputFile);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) > 0) {
            os.write(buffer, 0, length);
        }
    } finally {
        is.close();
        os.close();
    }
}    

private static void CarregarDados(){
    //metodo carregar os dados do ficheiro
System.out.println(localizacao);
int i=0;
try {
                         List<String> allLines = Files.readAllLines(Paths.get(localizacao));
                        for (String line : allLines) {
                            String [] vec = line.split(";");
                            VeiculoDados VeiculoD = null;
                            if (vec[4].equals("e")){
VeiculoD = new VeiculoDados(new MEletricoExtend(Integer.parseInt(vec[5]),Integer.parseInt(vec[6])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            else if (vec[4].equals("c")){                                
VeiculoD=new VeiculoDados(new MCombustivelExtend(Integer.parseInt(vec[5]),vec[6],Integer.parseInt(vec[7])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            StandVeic[i] = VeiculoD;
                                            i++;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println(" CarregarDados - O caminho não está correto!");
                    }
}

private static void Consultar(){
    //metodo de consultar os dados de cada stand
int i=0;
try {
                         List<String> allLines = Files.readAllLines(Paths.get(localizacao));
                        for (String line : allLines) {
                            String [] vec = line.split(";");
                            VeiculoDados VeiculoD = null;
                            if (vec[4].equals("e")){
VeiculoD = new VeiculoDados(new MEletricoExtend(Integer.parseInt(vec[5]),Integer.parseInt(vec[6])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            else if (vec[4].equals("c")){                                
VeiculoD=new VeiculoDados(new MCombustivelExtend(Integer.parseInt(vec[5]),vec[6],Integer.parseInt(vec[7])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            StandVeic[i] = VeiculoD;
                            System.out.println("\t"+"Veiculo Nº"+i+" - -"+StandVeic[i]);
                                            i++;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println(" Consultar - O caminho não está correto!");
                    }
}

    private static void Pesquisar() {
//chama classe exterior para pesquisar os dados pretendidos
PesquisarGlobal variavel = new PesquisarGlobal();
variavel.IniciarPesquisarGlobal(localizacao,NomeStand[NumeroStand]);
 
    }
}