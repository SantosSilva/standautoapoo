package standautoapoo;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

//classe para efetuar pesquisas
class PesquisarGlobal{

    private static final Scanner scan = new Scanner(System.in);
    public static String localizacao=null;
    public static VeiculoDados[] StandVeic = new VeiculoDados[50];
    private static MotorDadosIN engine = new MotorDadosIN();
    
    public static void IniciarPesquisarGlobal(String StandLocal, String NomStand) { // menu pesquisar

localizacao=StandLocal;
            System.out.println("\n\t#########################################################################");
            System.out.println("\t#    TRABALHO FINAL DE APOO - Grupo 17 (Tiago Silva e Rui Ferreira)     #");
            System.out.println("\t#########################################################################");
            System.out.println("\t                     Gestão do "+NomStand);
            System.out.println("\t#########################################################################");
            System.out.println("\t#  (1) Pesquisar veiculos com preço menor                                          #");
            System.out.println("\t#  (2) Pesquisar veiculos com preço maior                                   #");
            System.out.println("\t#  (3) Pesquisar veiculos com potencia superior                      #");
            System.out.println("\t#  (4) Pesquisar veiculos por tipo de motor                              #");
            System.out.println("\t#  (0) Sair                                                             #");
            System.out.println("\t#########################################################################");
            
        char escolher = scan.next().charAt(0);

            switch (escolher) {
                case '1' :
                    CheapCar();  
                    break;
                
                case '2' :
                    ExpensiveCar();
                    break;
                
                case '3' : 
                    PowerCar();
                    break;
                
                case '4' :    
                    EngineCar();
                    break;

                case '0' :                    
                    System.exit(0);

            default:  System.out.println("Opção indisponível!");
                    break;        
             }
}

private static void CheapCar() { // metodo ara pesquisar o veiculo com preço menor
System.out.println("\tInsira o valor maximo a pesquisar veiculos");
Double ler = scan.nextDouble();
int i = 0;
        try {
                         List<String> allLines = Files.readAllLines(Paths.get(localizacao));
                        for (String line : allLines) {
                            String [] vec = line.split(";");
                            VeiculoDados VeiculoD = null;
                            if (vec[4].equals("e") && Double.parseDouble(vec[3])<ler){
VeiculoD = new VeiculoDados(new MEletricoExtend(Integer.parseInt(vec[5]),Integer.parseInt(vec[6])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            else if (vec[4].equals("c") && Double.parseDouble(vec[3])<ler){                                
VeiculoD=new VeiculoDados(new MCombustivelExtend(Integer.parseInt(vec[5]),vec[6],Integer.parseInt(vec[7])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            StandVeic[i] = VeiculoD;
                            if(VeiculoD!=null){
                            System.out.println("\t"+"Veiculo Nº"+i+" - -"+StandVeic[i]);
                            }
                                            i++;
                        }
                    } catch (Exception e) {
                        System.out.println(" CheapCar - Contem erros!");
                    }
    }

private static void ExpensiveCar() {// metodo ara pesquisar o veiculo com preço superior
        
System.out.println("\tInsira o valor minimo a pesquisar veiculos");
Double ler = scan.nextDouble();
int i = 0;
        try {
                         List<String> allLines = Files.readAllLines(Paths.get(localizacao));
                        for (String line : allLines) {
                            String [] vec = line.split(";");
                            VeiculoDados VeiculoD = null;
                            if (vec[4].equals("e") && Double.parseDouble(vec[3])>ler){
VeiculoD = new VeiculoDados(new MEletricoExtend(Integer.parseInt(vec[5]),Integer.parseInt(vec[6])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            else if (vec[4].equals("c") && Double.parseDouble(vec[3])>ler){                                
VeiculoD=new VeiculoDados(new MCombustivelExtend(Integer.parseInt(vec[5]),vec[6],Integer.parseInt(vec[7])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            StandVeic[i] = VeiculoD;
                            if(VeiculoD!=null){
                            System.out.println("\t"+"Veiculo Nº"+i+" - -"+StandVeic[i]);
                            }
                                            i++;
                        }
                    } catch (Exception e) {
                        System.out.println(" ExpensiveCar - Contem erros!");
                    }
    }

private static void PowerCar() { // metodo ara pesquisar o veiculo com potencia igual ou superior

System.out.println("\tInsira a potencia minima");
Double ler = scan.nextDouble();
int i = 0;
        try {
                         List<String> allLines = Files.readAllLines(Paths.get(localizacao));
                        for (String line : allLines) {
                            String [] vec = line.split(";");
                            VeiculoDados VeiculoD = null;
                            if (vec[4].equals("e") && Double.parseDouble(vec[5])>=ler){
VeiculoD = new VeiculoDados(new MEletricoExtend(Integer.parseInt(vec[5]),Integer.parseInt(vec[6])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            else if (vec[4].equals("c") && Double.parseDouble(vec[5])>=ler){                                
VeiculoD=new VeiculoDados(new MCombustivelExtend(Integer.parseInt(vec[5]),vec[6],Integer.parseInt(vec[7])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            StandVeic[i] = VeiculoD;
                            if(VeiculoD!=null){
                            System.out.println("\t"+"Veiculo Nº"+i+" - -"+StandVeic[i]);
                            }
                                            i++;
                        }
                    } catch (Exception e) {
                        System.out.println(" PowerCar - Contem erros!");
                    }
    }

private static void EngineCar() { // metodo ara pesquisar o veiculo por tipo de moto

System.out.println("\tInsira o tipo motor");
Double ler = scan.nextDouble();
int i = 0;
        try {
                         List<String> allLines = Files.readAllLines(Paths.get(localizacao));
                        for (String line : allLines) {
                            String [] vec = line.split(";");
                            VeiculoDados VeiculoD = null;
                            if (vec[4].equals("e") && Double.parseDouble(vec[3])<ler){
VeiculoD = new VeiculoDados(new MEletricoExtend(Integer.parseInt(vec[5]),Integer.parseInt(vec[6])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            else if (vec[4].equals("c") && Double.parseDouble(vec[3])<ler){                                
VeiculoD=new VeiculoDados(new MCombustivelExtend(Integer.parseInt(vec[5]),vec[6],Integer.parseInt(vec[7])), vec[0], vec[1], Integer.parseInt(vec[2]), Double.parseDouble(vec[3]));
                            }
                            StandVeic[i] = VeiculoD;
                            if(VeiculoD!=null){
                            System.out.println("\t"+"Veiculo Nº"+i+" - -"+StandVeic[i]);
                            }
                                            i++;
                        }
                    } catch (Exception e) {
                        System.out.println(" EngineCar - Contem erros!");
                    }
    }
}
