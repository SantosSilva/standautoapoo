package standautoapoo;

public class MCombustivelExtend extends MotorDadosIN {
    
    private int cc;
    private String fueltype;
    
    /** parametros necessarios para veiculos a combustao
     *
     * @param powerCV parametro da potencia de veiculos a combustao ou eletricos
     * @param fueltype parametro dos tipos de combustiveis fosseis
     * @param cc parametro referente a cilindrado do motor do veiculo
     */
    public MCombustivelExtend( int powerCV , String fueltype , int cc ){
        super(powerCV, 'c');
        this.fueltype = fueltype;
        this.cc = cc;

    }

    /**
     *
     * @return tipo de combustivel utilizado
     */
    public String getFuelType(){
        return fueltype;
    }
    
    /**
     *
     * @param fueltype tipo de combustivel utilizado para cada veiculo sem referencias
     */
    public void setFuelType( String fueltype){
        this.fueltype = fueltype;
    }

    /**
     *
     * @return cilindrada de motor em cm3
     */
    public int getCC(){
        return cc;
    }
    
    /**
     *
     * @param cc
     */
    public void setCC(int cc){
        this.cc = cc;
    }
    
    /**toString
     * 
     * toString é usado para organizaçao e apresentacao
     * 
     * @return
     */
    public String toString(){
        return "||| Potencia CV: " + getPowerCV() + "CV ; Cilindrada: " + getCC() + "cm3 ; Tipo de Combustivel:" + getFuelType()+ " |||";
    }
    
    /**
     *
     * @return parametros necessarios para os motor de combustao
     */
    public String getSpecs() {
        return getPowerCV()+";"+getFuelType()+";"+getCC();
    }
}
