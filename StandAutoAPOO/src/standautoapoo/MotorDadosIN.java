package standautoapoo;

public class MotorDadosIN {
   private int powerCV;
    private char engine;
    
     /**Construtor de dados do motor
     *
     */
        public MotorDadosIN(){}
    
    /**
     *
     * @param powerCV potencia do veiculo
     * @param engine tipo de motor presente no veiculo
     */
    public MotorDadosIN(int powerCV, char engine){
        this.powerCV = powerCV;
        this.engine = engine;
    }

    /**
     *
     * @return potencia do motor do veiculo
     */
    public int getPowerCV() {
        return powerCV;
    }

    /**
     *
     * @param powerCV dados refertentes a potencia do veiculo
     */
    public void setPowerCV(int powerCV) {
        this.powerCV = powerCV;
    }

    /**
     *
     * @return parametros referentes ao motor
     */
    public char getEngineType() {
        return engine;
    }
    
    /**
     *
     * @return as especificaçoes do motor do veiculo dispostas neste projeto
     */
    public String getSpecs() {
        return "";
    }
    
    
    
}
