package standautoapoo;

public class VeiculoDados {
    
    private MotorDadosIN engine;
    private String brands = "";
    private String Registration = "";
    private int year = 0;
    private double price = 0;
    
    /**
     *
     * @param engine motor do veiculo
     * @param brands marca do veiculo
     * @param Registration matricula do veiculo
     * @param year ano do veiculo
     * @param price preço do veiculo
     */
    public VeiculoDados(MotorDadosIN engine, String brands, String Registration, int year, double price){
        setMotorDadosIN(engine);
        setBrands(brands);
        setCarRegistration(Registration);
        setYear(year);
        setPriceF(price);
    }

    /**
     *
     * @return dados do motor do veiculo
     */
    public MotorDadosIN getMotorDadosIN() {
        return engine;
    }

    /**
     *
     * @param engine parametros do motor para obter dados principais do veiculo
     */
    public void setMotorDadosIN(MotorDadosIN engine) {
        this.engine = engine;
    }
    
    /**
     *
     * @return marca do automovel
     */
    public String getBrands() {
        return brands;
    }

    /**
     *
     * @param brands parametros da marca
     */
    public void setBrands(String brands) {
        this.brands = brands;
    }

    /**
     *
     * @return matricula ou registro do veiculo
     */
    public String getCarRegistration() {
        return Registration;
    }

    /**
     *
     * @param Registration numeros e letras da matricula do veiculo
     */
    public void setCarRegistration(String Registration) {
        this.Registration = Registration;
    }

    /**
     *
     * @return data do primeiro registro do veiculo
     */
    public int getYear() {
        return year;
    }

    /**
     *
     * @param year dados do ano do veiculo
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     *
     * @return valor do veiculo
     */
    public double getPriceF() {
        return price;
    }

    /**
     *
     * @param price referencia ao preço do veiculo
     */
    public void setPriceF(double price) {
        this.price = price;
    }

    
       /** to string
     * toString é usado para o formato dos dados do veiculo
     * Método deixa os dados do veiculo organizados
     * 
     * @return
     */
    
    public String toString(){
        return "Marca: " + getBrands() + " ; Matricula: " + getCarRegistration() + " ; Preço: " + getPriceF() + "€ ; Ano: " + getYear() + " ; Motor: " + getMotorDadosIN();
    }
    
}
